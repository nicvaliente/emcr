# voicing_emcr

Voicing early, mid-career and senior scientists’ concerns about future career perspectives impeding their contribution to the long-term sustenance of Polar research: Identifying and addressing future post-pandemic global science barriers